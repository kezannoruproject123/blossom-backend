const express = require('express');
const cors = require('cors');
const { Pool } = require('pg');
const bcrypt = require('bcrypt');
const app = express();
const nodemailer = require('nodemailer');


app.use(cors());
app.use(express.json())

const transporter = nodemailer.createTransport({
  host: 'smtp.zoho.com',
  port: 465,
  secure: true,
  auth: {
    user: 'inspirational323@zohomail.com',
    pass: 'Shyam@15',
  },
});

const proConfig = {
  connectionString:
    "postgres://kezang:Kilte2mXofeoN5Je0OJxWXBE41JuNeD8@dpg-clbs8eccu2es73fja2s0-a.oregon-postgres.render.com/bb_db_96y6?ssl=true"
}

const devConfig={
  user: 'postgres',
  host: 'localhost',
  database: 'blossombouquet',
  password: 'kezangstrongman123',
  port: 5432, // Default PostgreSQL port
}
const pool=new Pool(proConfig)


app.post("/signup",async(req,res)=>{
    try{
        const { name, email, password } = req.body;
        if (!name || !email || !password) {
            return res.status(500).json({ error: 'Name, email, and password are required' });
          }
          const hashedPassword = await bcrypt.hash(password, 10);
          const insertQuery = await pool.query('INSERT INTO signup (name, email, password) VALUES ($1, $2, $3)', [name, email, hashedPassword]);

        res.status(200).json(insertQuery);

    }catch(err){
            console.log(err.message);
    }
})

app.get("/fetchdata",async(req,res)=>{
    try{
        const getQuery=await pool.query('SELECT * FROM signup');

        res.status(200).json(getQuery.rows)
        
    }catch(error){
        console.log(error)
    }
})

app.post('/login', async (req, res) => {
    try {
      const { email, password } = req.body;
      if (!email || !password) {
        return res.status(400).json({ error: 'Email and password are required' });
      }
  
      const result = await pool.query('SELECT * FROM signup WHERE email = $1', [email]);
      const user = result.rows[0];
      
      if (!user) {
        return res.status(401).json({ error: 'Invalid email or password' });
      }
  
      // Compare the provided password with the stored hashed password
      const passwordMatch = await bcrypt.compare(password, user.password);
  
      if (!passwordMatch) {
        return res.status(401).json({ error: 'Invalid email or password' });
      }
  
      res.status(200).json({ message: 'Login successful', user: { id: user.id, name: user.name, email: user.email } });
    } catch (err) {
      console.error(err.message);
      res.status(500).json({ error: 'Internal Server Error' });
    }
  });


// profile

app.get("/profiledata",async(req,res)=>{
  try{
      const getQuery=await pool.query('SELECT * FROM signup');

      res.status(200).json(getQuery.rows)
      
  }catch(error){
      console.log(error)
  }
})

function generatePassword(length) {
  const chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789#$%*&!';
  let password = '';
  for (let i = 0; i < length; i++) {
    password += chars.charAt(Math.floor(Math.random() * chars.length));
  }
  return password;
}


app.post('/forgotpassword', async (req, res) => {
  const { email } = req.body;

  const password = generatePassword(10);
  console.log(password);

  const saltRound = 10;
  const salt = await bcrypt.genSalt(saltRound);
  const bcryptPassword = await bcrypt.hash(password, salt);
  console.log(bcryptPassword);

  try {
    const user = await pool.query('SELECT * FROM signup WHERE email = $1', [email]);
    console.log(user)

    if (user.rows.length === 0) {
      // console.log('User not found');
      return res.status(404).json({ error: 'User not found' });
    }
    const mailOptions = {
      from: 'inspirational323@zohomail.com',
      to: email,
      subject: 'Password',
      text: `Your password is ${password}. Please login to your account and reset your password.`,
    };

    try {
      await transporter.sendMail(mailOptions);
      console.log('Email sent');

      const updatingPassword = await pool.query('UPDATE signup SET password = $1 WHERE email = $2 RETURNING *', [
        bcryptPassword,
        email,
      ]);

      if (updatingPassword.rows.length !== 0) {
        return res.status(200).json({ success: true, password: password });
      } else {
        return res.status(401).json({ error: 'Failed to update password' });
      }
    } catch (error) {
      console.log(error);
      return res.status(500).json({ error: 'Failed to send email' });
    }
  } catch (error) {
    console.log(error.message);
    return res.status(500).json({ error: error.message });
  }
});



  
app.listen(5000,()=>{
    console.log("Server running at",5000)
})


